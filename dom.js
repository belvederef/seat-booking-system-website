$(function(){
  $.ajax({url:'booking2.json',success:function(d){
    //Add elements that will be different according to the movie considered
    $('#screen').text(d.screen);
    $('#movie').text(d.title);
    $('#date').text($.format.date(d.date, 'dd/MM/yyyy hh:mma'));
    $('#rating').text(d.rating);
    $('#runtime').text(d.runtime);
    $('#poster').attr({"src":d.image,
      'width':'280px',
      'height':'400px'});

    //Build pricing panel
    $('#pricing').append('<p><b>Pricing:</b></p>')
                 .append('<p>Sofa seat: &pound' + d.pricing.L + '</p>')
                 .append('<p>Armchair seat: &pound' + d.pricing.A + '</p>');

    ///Build the left panel
    $('#seatSelection')
      .append('<p>Armchairs: <span id="armSeatsAlloc">0</span>/'
        + d.armchairRequire)
      .append('<p>Sofas: <span id="sofaSeatsAlloc">0</span>/'
        + d.sofaRequired);
    var t = $('<table/>').append('<tr/>').appendTo('#seatsSelected');
 
    for(var i=0; i<(d.armchairRequire+d.sofaRequired); i++)
	var td = $('<td/>');

    //Calculate total
    $('#seatSelection')
      .append('<p>Total: &pound <span id="total">0</span>');


    // The row loop - each step in this loop deals with one row of the
    // table and one row of the table
    var t = $('<table/>').appendTo('#theatreTab');
    
    for(var i=0;i<d.rowLabels.length;i++){
      // Create a table row
      var tr = $('<tr/>')
        .append($('<th/>',{text:d.rowLabels[i]}))
        .appendTo(t);

      // umap indicates if the seat is used
      // X indicates taken by someone else
      // O indicates available
      // M indicates my seat
      // space indicates no seat
      for(var j=0;j<d.umap[i].length;j++){
        var u0 = d.umap[i].charAt(j);
        // tmap indicates the type of seat
        // L or R for left or right sofa, A for armchair, space for none
        var t0 = d.tmap[i].charAt(j);
        var td = $('<td/>');

        //Left side combinations
        if (t0==='L')
        {
          if (u0==='O')
            td.addClass('left-sofa-free');
          if (u0==='X')
            td.addClass('left-sofa-taken');
          if (u0==='M')
            td.addClass('left-sofa-mine');
	}

        //Right side combinations
	if (t0==='R')
	{
          if (u0==='O')
            td.addClass('right-sofa-free');
          if (u0==='X')
            td.addClass('right-sofa-taken');
          if (u0==='M')
            td.addClass('right-sofa-mine');
	}

	//Armchair combinations
	if (t0==='A')
	{
          if (u0==='O')
            td.addClass('armchair-free');
          if (u0==='X')
            td.addClass('armchair-taken');
          if (u0==='M')
            td.addClass('armchair-mine');
	}

	//Show seat number
        if (t0!==' ')
	{
	  td.text(j+1)
	    .css({'font-size':'small', 'text-align':'center'});
	}

        tr.append(td);
      }
    tr.append($('<th/>',{text:d.rowLabels[i]}));
    }

    //Give visual hint about seat prices
    $('td.left-sofa-free, td.right-sofa-free')
      .attr('title','\u00A3' + d.pricing.L);
    $('td.armchair-free').attr('title', '\u00A3' + d.pricing.A);

    //Values to store number of selected seats
    var bs = $('td.left-sofa-mine').length + $('td.right-sofa-mine').length;
    var ba = $('td.armchair-mine').length;

    //Define the action to be taken when the user clicks on a seat
    $('td').click(function(){
      if ($(this).attr('class').indexOf('armchair-free') >= 0
          && ba < d.armchairRequire)
      {
        $(this).addClass('armchair-mine').removeClass('armchair-free');
      }
      else if ($(this).attr('class').indexOf('armchair-mine') >= 0)
      {
        $(this).removeClass('armchair-mine').addClass('armchair-free');
      }
      else if ($(this).attr('class').indexOf('left-sofa-free') >= 0
               && bs < d.sofaRequired)
      {
        $(this).addClass('left-sofa-mine').removeClass('left-sofa-free');
        $(this).next()
               .addClass('right-sofa-mine')
               .removeClass('right-sofa-free');
      }
      else if ($(this).attr('class').indexOf('left-sofa-mine') >= 0)
      {
        $(this).addClass('left-sofa-free').removeClass('left-sofa-mine');	
        $(this).next()
               .addClass('right-sofa-free')
               .removeClass('right-sofa-mine');	
      }
      else if ($(this).attr('class').indexOf('right-sofa-free') >= 0
	       && bs < d.sofaRequired)
      {
        $(this).addClass('right-sofa-mine').removeClass('right-sofa-free');
        $(this).prev().addClass('left-sofa-mine').removeClass('left-sofa-free');
      }
      else if ($(this).attr('class').indexOf('right-sofa-mine') >= 0)
      {
        $(this).addClass('right-sofa-free').removeClass('right-sofa-mine');	
        $(this).prev().addClass('left-sofa-free').removeClass('left-sofa-mine');
      }
      else if (bs === d.sofaRequired || ba === d.armchairRequire)
	alert('All seats of this type have been chosen!');

      //Refresh seats selected values
      bs = $('td.left-sofa-mine, td.right-sofa-mine').length;
      ba = $('td.armchair-mine').length;

      //Refresh infobox values
      $('#sofaSeatsAlloc').text(bs);
      $('#armSeatsAlloc').text(ba);

      //Update total
      $('#total').text((bs*d.pricing.L + ba*d.pricing.A).toFixed(2));

      //Check whether it is possible to continue - give visual feedback
      if (bs === d.sofaRequired && ba === d.armchairRequire)
        $('#continue').removeClass('disabled').addClass('hovered');
      else 
        $('#continue').removeClass('hovered').addClass('disabled');

      //List selected seasts on the right panel
      var ms = $('td.armchair-mine, td.left-sofa-mine, td.right-sofa-mine');
      var sl = ms.map(function(){
        return ($(this).text() + '-' + $(this).parent().text().substr(0, 2));
      });
      var ul = $('<ul/>');    
      for(var i=0;i<sl.length;i++){
        ul.append($('<li/>',{text:sl[i]}));
      }
      $('#seatSelection ul').remove();	
      $('#seatSelection').append(ul);	
    });
  }})
});
